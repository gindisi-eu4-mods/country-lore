defined_text = { #The custom loc triggers for Anbennar exist to bypass the text for later ages replacing the default start text in later years. AB and Anbennar should remove those from their custom loc - they aren't necessary and it would improve compatibility with this mod.
    name = CountryLoreDesc
	random = no
	text = {
        localisation_key = country_lore_desc_anbennar
        trigger = {
			OR = {
				is_mod_active = "Anbennar"
				is_mod_active = "Anbennar-PublicFork"
				is_mod_active = "Anbennar: A Fantasy Total Conversion"
				is_mod_active = "Anbennar MP modified"
			}
        }
    }
    text = { #Vanilla will still display the descriptions for later ages
        localisation_key = country_lore_desc_generic
        trigger = {
			always = yes
        }
    }
}
defined_text = {
    name = AnbennarCountryLoreDesc
	random = no
	text = { #Lorent
		localisation_key = string_start_lorent
		trigger = {
			tag = A01
		}
	}
	text = { #Wesdam
		localisation_key = string_start_wesdam
		trigger = {
			tag = A04
		}
	}
	text = { #Reveria
		localisation_key = string_start_reveria
		trigger = {
			tag = A05
		}
	}
	text = { #Adshaw
		localisation_key = string_start_adshaw
		trigger = {
			tag = Z26
		}
	}
	text = { #Gawed
		localisation_key = string_start_gawed
		trigger = {
			tag = A13
		}
	}
	text = { #Wex
		localisation_key = string_start_wex
		trigger = {
			tag = A30
		}
	}
	text = { #Verne
		localisation_key = string_start_verne
		trigger = {
			tag = A33
		}
	}
	text = { #Istralore
		localisation_key = string_start_istralore
		trigger = {
			tag = A45
		}
	}
	text = { #Bjarnrik
		localisation_key = string_start_Bjarnrik
		trigger = {
			tag = Z08
		}
	}
	text = { #Urviksten
		localisation_key = string_start_Urviksten
		trigger = {
			tag = Z15
		}
	}
	#Bulwar
	#Countries
	text = { #Birsartanses
		localisation_key = string_start_Birsartanses
		trigger = {
			tag = F21
		}
	}

	text = { #Dartaxagerdim
		localisation_key = string_start_Dartaxagerdim
		trigger = {
			tag = F22
		}
	}

	text = { #Sareyand
		localisation_key = string_start_Sareyand
		trigger = {
			tag = F25
		}
	}

	text = { #Ayarallen
		localisation_key = string_start_Ayarallen
		trigger = {
			tag = F27
		}
	}

	text = { #Tluukt
		localisation_key = string_start_Tluukt
		trigger = {
			tag = F28
		}
	}

	text = { #Zokka
		localisation_key = string_start_Zokka
		trigger = {
			tag = F29
		}
	}

	text = { #Azka-Evran
		localisation_key = string_start_AzkaEvran
		trigger = {
			tag = F34
		}
	}

	text = { #Irrliam
		localisation_key = string_start_Irrliam
		trigger = {
			tag = F37
		}
	}

	text = { #Varamhar
		localisation_key = string_start_Varamhar
		trigger = {
			tag = F42
		}
	}

	text = { #The Jadd
		localisation_key = string_start_Jaddari
		trigger = {
			tag = F46
		}
	}

	text = { #Mulen
		localisation_key = string_start_Mulen
		trigger = {
			tag = F50
		}
	}

	#Aelantir
	text = { #Malacnar
		localisation_key = string_start_malacnar
		trigger = {
			tag = G32
		}
	}
	#Aelantir
	text = { #Arverynn
		localisation_key = string_start_Arverynn
		trigger = {
			tag = G35
		}
	}

	#Aelantir
	text = { #Brelar
		localisation_key = string_start_brelar
		trigger = {
			tag = G27
		}
	}

	text = { #Elizna
		localisation_key = string_start_Elizna
		trigger = {
			tag = U05
		}
	}

	text = { #Re'uyel
		localisation_key = string_start_Reuyel
		trigger = {
			tag = U18
		}
	}

	text = { #Azka-Sur
		localisation_key = string_start_AzkaSur
		trigger = {
			tag = U20
		}
	}

	text = { #Azkare
		localisation_key = string_start_Azkare
		trigger = {
			tag = Y86
		}
	}
	

	text = { #Dhenijanraj
		localisation_key = string_start_Dhenijanraj
		trigger = {
			tag = R51
		}
	}

	text = {
		localisation_key = string_start_Azjakuma
		trigger = {
			tag = Y01
		}
	}

	text = { # Feiten
		localisation_key = string_start_feiten
		trigger = {
			tag = Y20
		}
	}

	text = { # Bianfang
		localisation_key = string_start_Bianfang
		trigger = {
			tag = Y09
		}
	}

	text = { # Chien Binhrung
		localisation_key = string_start_chienbinhrung
		trigger = {
			tag = Y72
		}
	}

	# Adventurer Companies in Escann
	text = {
		localisation_key = string_start_escann_adventurer
		trigger = {
			AND = {
				capital_scope = {
					superregion = escann_superregion
				}
				government = theocracy
			}
		}
	}

	#Marrhold and Count's League
	text = {
		localisation_key = string_start_escann_nonadventurer
		trigger = {
			OR = {
				tag = B55 # Count's League
				tag = B36 # Marrhold
			}
		}
	}


	#Regions, leave this before the Countries otherwise this will be shown instead (this is a backup shown for countries who dont have special intros
	text = {
		localisation_key = string_start_tag_bahar
		trigger = {
			capital_scope = {
				region = bahar_region
			}
		}
	}

	text = {
		localisation_key = string_start_tag_ourdia
		trigger = {
			capital_scope = {
				region = ourdia_region
			}
		}
	}

	text = { #Kheterata
		localisation_key = string_start_kheterata
		trigger = {
			tag = U01
		}
	}

	text = { #Nirat
		localisation_key = string_start_nirat
		trigger = {
			tag = U02
		}
	}

	text = { # Viakkoc
		localisation_key = string_start_viakkoc
		trigger = {
			tag = U07
		}
	}

	text = {
	localisation_key = string_start_northern_isles_description
	trigger = {
			capital_scope = {
				region = northern_isles_region
			}
		}
	}

	text = {
	localisation_key = string_start_southern_isles_description
	trigger = {
			capital_scope = {
				region = southern_isles_region
			}
		}
	}

	text = {
	localisation_key = string_start_ogre_kingdoms_description
	trigger = {
			capital_scope = {
				region = ogre_valley_region
			}
		}
	}

	text = {
	localisation_key = string_start_dwarovar_adventurer_description
	trigger = {
			AND = {
				capital_scope = {
					OR = {
						superregion = west_serpentspine_superregion
						superregion = east_serpentspine_superregion
					}
				}
				government = theocracy
				culture_group = dwarven
			}
		}
	}

	text = {
		localisation_key = string_start_dwarovar_remnant_description
		trigger = {
			AND = {
				capital_scope = {
					OR = {
						superregion = west_serpentspine_superregion
						superregion = east_serpentspine_superregion
					}
				}
				NOT = { government = theocracy }
				culture_group = dwarven
			}
		}
	}

	text = {
		localisation_key = string_start_cheshohi_description
		trigger = {
			religion = death_cult_of_cheshosh
		}
	}

	text = {
		localisation_key = string_start_cave_goblin_description
		trigger = {
			dwarovar_goblin_culture_primary = yes
		}
	}

	text = {
		localisation_key = string_start_nimscodd_description
		trigger = {
			primary_culture = cliff_gnome
		}
	}

	text = {
		localisation_key = string_start_deepwoods_elves
		trigger = {
			AND = {
				capital_scope = {
					superregion = deepwoods_superregion
				}
				primary_culture = wood_elf
			}
		}
	}

	text = {
		localisation_key = string_start_deepwoods_goblins
		trigger = {
			AND = {
				capital_scope = {
					superregion = deepwoods_superregion
				}
				primary_culture = forest_goblin
			} # sneaky groundhawk
		}
	}

	text = {
		localisation_key = string_start_deepwoods_orcs
		trigger = {
			AND = {
				capital_scope = {
					superregion = deepwoods_superregion
				}
				primary_culture = green_orc
			}
		}
	}

	text = {
		localisation_key = string_start_bulwar_generic_description
		trigger = {
				capital_scope = {
					superregion = bulwar_superregion
			}
		}
	}
	text = {
		localisation_key = string_start_Rahen
		trigger = {
			capital_scope = {
				superregion = rahen_superregion
			}
		}
	}
	text = {
		localisation_key = string_start_kheionai
		trigger = {
			culture_group = kheionai_ruinborn_elf
		}
	}
	#Superregions, leave this before the Countries otherwise this will be shown instead (this is a backup shown for countries who dont have special intros)
	text = {
	localisation_key = string_start_forbidden_lands
	trigger = {
			capital_scope = {
				superregion = forbidden_lands_superregion
			}
		}
	}

	text = {
		localisation_key = string_start_eordand_spring
		trigger = {
			AND = {
				capital_scope = {
					superregion = eordand_superregion
				}
				religion = spring_court
			}
		}
	}
	text = {
		localisation_key = string_start_eordand_summer
		trigger = {
			AND = {
				capital_scope = {
					superregion = eordand_superregion
				}
				religion = summer_court
			}
		}
	}
	text = {
		localisation_key = string_start_eordand_autumn
		trigger = {
			AND = {
				capital_scope = {
					superregion = eordand_superregion
				}
				religion = autumn_court
			}
		}
	}
	text = {
		localisation_key = string_start_eordand_winter
		trigger = {
			AND = {
				capital_scope = {
					superregion = eordand_superregion
				}
				religion = winter_court
			}
		}
	}
	text = {
		localisation_key = string_start_eordand_eordellon
		trigger = {
			AND = {
				capital_scope = {
					superregion = eordand_superregion
				}
				religion = eordellon
			}
		}
	}
	#Continents, leave this before the Countries otherwise this will be shown instead (this is a backup shown for countries who dont have special intros
		text = {
		localisation_key = string_start_tag_cannor
		trigger = {
			capital_scope = {
				continent = europe
			}
		}
	}
	text = {#Fallback
		localisation_key = string_start_tag_generic
		trigger = {
			always = yes
		}
	}
}