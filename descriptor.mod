name="Country Lore"
version="1.0.3"
picture="thumbnail.png"
dependencies={
	"Anbennar: A Fantasy Total Conversion"
	"Anbennar-PublicFork"
	"Anbennar"
	"Anbennar MP modified"
}
tags={
	"Utilities"
}
supported_version="v1.37.*.*"
remote_file_id="3123368644"